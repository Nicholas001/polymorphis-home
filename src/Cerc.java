public class Cerc implements Forme {

    private double raza;

    public double getRaza() {
        return raza;
    }

    protected void setRaza(double raza) {
        this.raza = raza;
    }

    public Cerc() {
    }

    public Cerc(double raza) {
        this.raza = raza;
    }

    @Override
    public void calculeazaAria (){
        double aria = 3.14 * (raza * raza);
        System.out.println("Raza cercului este " + aria);
    }

}

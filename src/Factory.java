public class Factory {

    public Forme getForma(String forma, int... parametru) {

        if (forma.equalsIgnoreCase("patrat")) {
            return new Patrat(parametru[0]);
        } else if (forma.equalsIgnoreCase("dreptunghi")) {
            return new Dreptunghi(parametru[0],parametru[1]);
        } else if (forma.equalsIgnoreCase("cerc")) {
            return new Cerc(parametru[0]);
        }
        return null;
    }
}
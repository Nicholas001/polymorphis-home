public class Patrat implements Forme{

    private int latura;

    public Patrat() {
    }

    public Patrat(int latura) {
        this.latura = latura;
    }

    @Override
    public void calculeazaAria (){

        int aria = latura * latura;
        System.out.println("Aria patratului este " + aria);

    }


}

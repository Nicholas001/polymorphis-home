public class Dreptunghi implements Forme {

    private int lungime;
    private int latime;

    public Dreptunghi() {
    }

    public Dreptunghi(int lungime, int latime) {
        this.lungime = lungime;
        this.latime = latime;
    }

    @Override
    public void calculeazaAria (){
        int aria = latime * lungime;
        System.out.println("Aria dreptunghiului este " + aria);
    }

}
